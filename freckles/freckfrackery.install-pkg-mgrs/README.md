freckfrackery.install_vagrant
=============================

Downloads the appropriate Vagrant package, installs it.

Note: for now download happens with the get_url module, setting 'validate_certs' to 'false', because of this issue with Ansible 2.3: https://github.com/ansible/ansible/pull/26235


Role Variables
--------------

    vagrant_force_update: false  # update Vagrant, even if it is already installed
    vagrant_version: 2.1.2  # the version of Vagrant to install
    vagrant_download_url: https://releases.hashicorp.com/vagrant  # the base download url
    vagrant_download_path: "/tmp/_vagrant_download"  # the tempoary download folder
    vagrant_delete_download_after: yes  # whether to delete the downloaded install file after successful install


Dependencies
------------

curl (on Mac OS X) -- get_url errors out for some reason, at least on El Capitan

Example Playbook
----------------

    - hosts: all
      tasks:
         - include_role:
             name: freckfrackery.install-vagrant
           vars:
             vagrant_version: 2.1.2


License
-------

[GPLv3][license]

Author Information
------------------

Author:: [Markus Binsteiner][freckfrackery] <[makkus@frkl.io](makkus@frkl.io)>


[freckfrackery]: https://gitlab.com/freckfrackery
[license]: https://www.gnu.org/licenses/gpl.txt
