# -*- coding: utf-8 -*-

import copy

from ansible.module_utils.six import iteritems


class FilterModule(object):

    def filters(self):
        return {"filter_required_pkg_mgrs": self.filter_required_pkg_mgrs}

    def filter_existing_pkg_mgrs(self, pkg_mgrs, freckles_facts):

        result = {}
        for pkg_mgr, details in iteritems(pkg_mgrs):

            fpm = freckles_facts["pkg_mgrs"].get(pkg_mgr, None)
            if fpm is None:
                result[pkg_mgr] = details

            if not fpm["is_available"]:
                result[pkg_mgr] = details

            # TODO: check package manager dependencies, like python-apt

        return result

    def filter_required_pkg_mgrs(
        self, package_list, init_pkg_mgrs={}, freckles_facts=None
    ):

        result = copy.deepcopy(init_pkg_mgrs)
        for pkg_item in package_list:
            pkg_mgr = pkg_item["pkg_mgr"]
            pkg_become = pkg_item["become"]

            # in case pkg_mgr needs both become and not become, we go
            # with become
            if pkg_mgr not in result.keys() or result[pkg_mgr]["become"] == False:
                result[pkg_mgr] = {"become": pkg_become}
            result[pkg_mgr].setdefault("packages", []).append(pkg_item)

        if freckles_facts is not None:
            result = self.filter_existing_pkg_mgrs(
                result, freckles_facts=freckles_facts
            )

        return result
